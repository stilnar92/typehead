import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

import { User } from '../model';
import UserItem from './UserItem';
import Footer from './Footer';

import {
  SHOW_ACTIVE,
  SHOW_ALL
} from '../../constants/UserFilters';

import {
  inputUser,
  clearUsers,
} from '../../typehead';

const TODO_FILTERS = {
  [SHOW_ACTIVE]: user => user.active == true,
  [SHOW_ALL]: () => true,
};

interface MainSectionProps {
  users: User[];
  actions: any;
};

interface MainSectionState {
  filter: string;
};

class MainSection extends React.Component<MainSectionProps, MainSectionState> {
  constructor(props, context) {
    super(props, context);
     this.state = { filter: SHOW_ALL };
  }
  renderFooter() {
    const { users } = this.props;
    const { filter } = this.state;
    if (users.length) {
      return (
        <Footer filter={filter}
                onShow={this.handleShow.bind(this)} />
      );
    }
  }
  handleShow(filter) {
    this.setState({ filter });
  }
  render() {
    const { users} = this.props;
    const { inputUser, clearUsers} = this.props.actions;
    const { filter } = this.state;
    const filteredUsers = users.filter(TODO_FILTERS[filter]);
    return (
      <section className="main">
        <ul className="todo-list">
          {filteredUsers.map(user =>
            <UserItem
              key={user.id}
              user={user}
              writeUserInInput={inputUser}
              clearUsers={clearUsers}
             />
          )}
        </ul>
        {this.renderFooter()}
      </section>
    );
  }
}

const mapStateToProps = state => ({
  users: state.users,
});

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({inputUser, clearUsers}, dispatch) 
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(MainSection);

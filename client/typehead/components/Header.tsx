import * as React from 'react';
import SearchUsersInput from './searchUsersInput';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
interface HeaderProps {
  actions: any;
  user: string
};
import {
  searchUsers,
  inputUser,
  model,
} from '../../typehead';

class Header extends React.Component<HeaderProps, void> {
  constructor(props, context) {
    super(props, context);
  }
  handleInputUser(text: string) {
    this.props.actions.inputUser(text);
    this.props.actions.searchUsers(text);
  }
  render() {
    return (
      <header className="header">
        <h1>Search</h1>
        <SearchUsersInput
            onChange={this.handleInputUser.bind(this)}
            placeholder="Search users" 
            text={this.props.user}/>
      </header> 
    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({inputUser, searchUsers}, dispatch) 
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);

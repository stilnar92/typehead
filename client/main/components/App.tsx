import { Dispatch, bindActionCreators, Action } from 'redux';
import { connect } from 'react-redux';
import * as React from 'react';

import {
  Header,
  MainSection,
} from '../../typehead';

class App extends React.Component<void, void> {
  render() {
    return (
      <div className="todoapp">
        <Header />
        <MainSection/>
      </div>
    );
  }
}

export default App;
